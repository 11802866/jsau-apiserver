'use strict'

const request = require('supertest')
const app = require('./api.js')

describe('API Tests', () => {
    let server

    beforeAll(() => {
        server = app.listen(3001)
    })

    afterAll(() => {
        return new Promise((resolve) => server.close(resolve))
    })

    it('should return all messages with a GET request', async() => {
        const res = await request(app).get('/messages')
        expect(res.status).toBe(200)
    })

    it('should create a new message with a POST request', async() => {
        const newMessage = {
            author: 'John_Doe',
            text: 'Hello, World!',
        }

        const res = await request(app).post('/messages').send(newMessage)
        expect(res.status).toBe(201)
    })

    it('should delete a message with a DELETE request', async() => {
        const newMessage = {
            author: 'Jane_Doe',
            text: 'Testing delete functionality',
        }

        const createRes = await request(app).post('/messages').send(newMessage)

        const messageId = createRes.body.id

        const deleteRes = await request(app).delete(`/messages/${messageId}`)

        expect(deleteRes.status).toBe(200)

        const getRes = await request(app).get('/messages')

        expect(getRes.text).not.toContain(`ID : ${messageId}`)
    })

})

