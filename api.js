'use strict'
const express = require('express')
const bodyParser = require('body-parser')
const fs = require('fs').promises

const app = express()
app.use(bodyParser.json())

app.use((req, res, next) => {
    res.setHeader('Cache-Control', 'no-cache, no-store, must-revalidate')
    next()
})

const messagesFilePath = 'messages.json'
const messages = []
let lastMessageId = 0

async function saveMessagesToFile() {
    try {
        await fs.writeFile(messagesFilePath, JSON.stringify(messages, null, 2))
    } catch (error) {
        console.error('Erreur lors de l\'enregistrement des messages dans le fichier :', error)
        throw error
    }
}

const emojiMapping = {
    ':grinning:': '😀',
    ':blush:': '😊',
    ':thumbsup:': '👍',
    ':heart:': '❤️',
    ':fire:': '🔥',
    ':rocket:': '🚀',
    ':eyes:': '👀',
}

function replaceEmojis(text) {
    for (const [key, value] of Object.entries(emojiMapping)) {
        text = text.replace(new RegExp(key, 'g'), value)
    }
    return text
}

async function loadMessagesFromFile() {
    try {
        const data = await fs.readFile(messagesFilePath, 'utf8')
        const savedMessages = JSON.parse(data)
        messages.push(...savedMessages)

        lastMessageId = Math.max(...savedMessages.map((message) => message.id), 0)
        console.log('Messages chargés depuis le fichier.')
    } catch (error) {
        console.error('Erreur lors du chargement des messages depuis le fichier :', error)
        throw error
    }
}

loadMessagesFromFile()

app.get('/messages', (req, res) => {
    const formattedMessages = messages.map((message) => `ID : ${message.id}<br>Auteur : ${message.author}<br>Message : ${replaceEmojis(message.text)}<br><br>`)
    res.send(formattedMessages.join('\n'))
})

app.post('/messages', async(req, res) => {
    const {author, text} = req.body

    if (!author || !text) {
        return res.status(400).json({error: 'Author and text are required'})
    }

    const newMessage = {
        id: ++lastMessageId,
        author,
        text: replaceEmojis(text),
        timestamp: new Date(),
    }

    messages.push(newMessage)
    await saveMessagesToFile()
    res.status(201).json(newMessage)
})

app.delete('/messages/:id', (req, res) => {
    const messageId = parseInt(req.params.id)

    if (isNaN(messageId) || messageId <= 0) {
        return res.status(400).json({error: 'Invalid message ID'})
    }

    const messageIndex = messages.findIndex((message) => message.id === messageId)

    if (messageIndex === -1) {
        return res.status(400).json({error: 'Message not found'})
    }

    const deletedMessage = messages.splice(messageIndex, 1)[0]

    saveMessagesToFile()

    res.json(deletedMessage)
})

module.exports = app

