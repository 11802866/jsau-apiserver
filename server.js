'use strict'
const app = require('./api.js')
const port = 3000

app.listen(port, () => {
    console.log(`Serveur API écoutant sur le port ${port}`)
})

